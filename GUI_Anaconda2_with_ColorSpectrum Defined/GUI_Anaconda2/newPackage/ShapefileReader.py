#!/usr/bin/env python
# -*- coding: 'iso8859-15' -*-
import shapefile 
import matplotlib.pyplot as plt


def GetFigure():

    shpFilePath = "D:\One drive@D\SOFTWARE LAB\commuteData\shp\VG250_Bundeslaender.shp"

    test=shapefile.Reader(shpFilePath)

    shapes=test.shapes()


    for nStates in (5,11,19):   # for all shapes in the shapefile
    
        if len(shapes[nStates].parts)==1:   #if shape has only one polygon

            listX=[]                        #create empty list for x-Koordinates
            listY=[]                        #create empty list for x-Koordinates

            for nPoints in xrange(0,len(shapes[nStates].points)):       #stores points in list

                listX.append(shapes[nStates].points[nPoints][0]) 
                listY.append(shapes[nStates].points[nPoints][1])
            
            plt.plot(listX,listY) 
            plt.fill(listX,listY)

        else:                                                #if shapes has more than one polygons
       
            for nPolygons in xrange(0,len(shapes[nStates].parts)-1):   #for every polygon

                listX=[]                        #create empty list for x-Koordinates
                listY=[]                        #create empty list for x-Koordinates             

                for nPoints in xrange(shapes[nStates].parts[nPolygons] , shapes[nStates].parts[nPolygons+1]):

                    listX.append(shapes[nStates].points[nPoints][0]) 
                    listY.append(shapes[nStates].points[nPoints][1])

                plt.plot(listX,listY) 
                plt.fill(listX,listY)



                listX=[]                        #create empty list for x-Koordinates
                listY=[]                        #create empty list for x-Koordinates             

                n = len(shapes[nStates].parts)
                nP = len(shapes[nStates].points)

                for nPoints in xrange(shapes[nStates].parts[n-1] , nP):

                    listX.append(shapes[nStates].points[nPoints][0]) 
                    listY.append(shapes[nStates].points[nPoints][1])

                plt.plot(listX,listY) 
                plt.fill(listX,listY)
         
  
    fig=plt.figure(1)

    
    return fig

def addAllKreiseShapes():
    shpFilePathGemeinden = "D:\One drive@D\SOFTWARE LAB\commuteData\shp\VG250_Kreise.shx"
    YYY=shapefile.Reader(shpFilePathGemeinden)
    shapes=YYY.shapes()
    records=YYY.records()

    for i in xrange(0,len(shapes)):
        listX=[]                        #create empty list for x-Koordinates
        listY=[]
        if (int(records[i][7])>8999999 and int(records[i][7])<10000000): 
            for j in xrange(0,len(shapes[i].points)):
                listX.append(shapes[i].points[j][0]) 
                listY.append(shapes[i].points[j][1])
            plt.plot(listX,listY)
            plt.fill(listX,listY)
    fig=plt.figure(1)
    
 
 
 

    return fig 
    


def addGemeinde(Listplace):
    shpFilePathGemeinden = "D:\One drive@D\SOFTWARE LAB\commuteData\shp\VG250_Gemeinden.shx"

    YYY=shapefile.Reader(shpFilePathGemeinden)

    listX=[]                        #create empty list for x-Koordinates
    listY=[]                        #create empty list for x-Koordinates

    for j in xrange(0,len(YYY.shape(Listplace).points)):
        listX.append(YYY.shape(Listplace).points[j][0]) 
        listY.append(YYY.shape(Listplace).points[j][1])
    plt.plot(listX,listY)
    fig=plt.figure(1)

    return fig 

def arrowFig(xR,yR,xW,yW):
    dx=xW-xR
    dy=yW-yR


    plt.arrow(xR, yR, dx, dy, head_width=0.05, head_length=0.1, fc='k', ec='k')
    plt.plot()
    fig=plt.figure(1)

    return fig
   

def computeCenter(Listplace):
    shpFilePathGemeinden = "D:\One drive@D\SOFTWARE LAB\commuteData\shp\VG250_Gemeinden.shx"

    YYY=shapefile.Reader(shpFilePathGemeinden)
    

    shape=YYY.shape(Listplace).bbox
    x1=shape[0]
    x2=shape[2]
    y1=shape[1]
    y2=shape[3]
    x=x1+(x2-x1)/2
    y=y1+(y2-y1)/2
 


    return x,y

def getGemeindeListe():
    Liste=[]
    shpFilePathGemeinden = "D:\One drive@D\SOFTWARE LAB\commuteData\shp\VG250_Gemeinden.shx"

    YYY=shapefile.Reader(shpFilePathGemeinden)
    records=YYY.records()
   
    for i in xrange(0,len(records)):
        x=records[i][5].decode('iso8859-15')
        Liste.append(x)
    
    return Liste

def getListplace(Liste,Gemeinde):
    result=""
    for i in xrange(0,len(Liste)):
        if Liste[i]==Gemeinde:
            result=i
            break
    return result


















