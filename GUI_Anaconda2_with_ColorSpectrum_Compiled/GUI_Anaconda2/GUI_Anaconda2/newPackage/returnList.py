import csv


def returnList():
    with open("output_data.csv") as csvfile:
        list=[]
        i=0
        readCSV = csv.reader(csvfile, delimiter=" ")
        for row in readCSV:
            list.append([])
            
            list[i].append(row[0])
            list[i].append(row[1])
            list[i].append(row[2])
            i=i+1
    return list