from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg, NavigationToolbar2TkAgg
from Tkinter import *
from newPackage import GUI2HelperFunctions
from newPackage import ShapefileReader





class KreisePage:
    ListeShapes=[]
    ListeRecords=[]
    In = True
    county = False
    

    def __init__(self,master):
        self.ListeShapes, self.ListeRecords=GUI2HelperFunctions.getListPlaceOfAllMunicipalitiesInBavaria()
        
        
        self.frame=Frame(master)
        self.frame.pack() 
       
        self.f = ShapefileReader.GetFigure()   
        self.plot = FigureCanvasTkAgg(self.f , master=self.frame)
        self.plot.get_tk_widget().grid(row=3, column = 0,columnspan=30)
        
       #toolbar_frame=Frame(self.frame)
       #toolbar_frame.grid(row=0, column=30, )
        
        
        SelectMunicipality=Button(self.frame,text="Municipality",command=self.showMunicipality)
        SelectMunicipality.grid(row=0, column=0) 
        
        SelectKreise=Button(self.frame,text="Kreise", command=self.showCounties)
        SelectKreise.grid(row=1)
        
        SelectIn=Button(self.frame,text="In Commuters", command=self.InCommuters)  
        SelectIn.grid(row=0, column=1) 
        
        SelectOut=Button(self.frame,text="Out Commuters", command=self.OutCommuters)
        SelectOut.grid(row=1, column=1)
        
        
        
        
    def showMunicipality(self):
        print("Function: showMunicipality Called")
        self.county=False
        self.mun = GUI2HelperFunctions.getMun(self.ListeShapes,self.ListeRecords,self.In)
        #print(len(self.ListeRecords)) //Prints length of ListRecords
        #print(self.ListeShapes)    //Print the shape records
        #print(self.ListeRecords)    //Prints the List records
        '''Set of Codes that writes ListShapes and ListRecords to text files'''      
#         f = open('ListShapes', 'w') #opens a file named ListShapes which can be written
#         value=self.ListeShapes      #gets value of List shapes
#         s=str(value)                #converts the value into string
#         f.write(s)                  #Writes the string into the files
#         
#         f= open ('ListRecords', 'w')
#         f.write(str(self.ListeRecords))
#         print("Written files List records and list shapes")"""
        
        self.plot = FigureCanvasTkAgg(self.mun , master=self.frame)
        self.plot.get_tk_widget().grid(row=3, column = 0,columnspan=30)
        self.plot.show()
        
    def showCounties(self):
        print("Function: showCounties Called")
        
        self.county=True
        self.c = GUI2HelperFunctions.getCounties(self.In)
        self.plot = FigureCanvasTkAgg(self.c , master=self.frame)
        self.plot.get_tk_widget().grid(row=3, column = 0,columnspan=30)
        

        self.plot.show()
    
    def InCommuters(self):
        print("Function: InCommuters Called")
        self.In=True
        if self.county==True:
            self.showCounties()
        if self.county==False:
            self.showMunicipality()
    
    def OutCommuters(self):
        print("Function: OutCommuters Called")
        self.In=False
        if self.county==True:
            self.showCounties()
        if self.county==False:
            self.showMunicipality()
        




