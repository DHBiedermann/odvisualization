#!/usr/bin/env python
# -*- coding: 'iso8859-15' -*-
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg, NavigationToolbar2TkAgg
from Tkinter import *
from newPackage import GUI2HelperFunctions
from newPackage import shapefile
import xlrd
import numpy as np
from doctest import master
import csv
import code
import matplotlib as mpl
from matplotlib import pyplot
from ipykernel.comm.comm import Comm
'''
<<<<<<< HEAD
from bokeh.charts.builders.area_builder import Area

=======
import matplotlib as mpl
from matplotlib import pyplot
>>>>>>> 682155244d924e224a557280b282b0a40313033d
'''

class KreisePage:
    ListeShapes=[]
    ListeRecords=[]
    
    In = True
    Out=False
    Net=False
    
    
    county = False
    normalize_Area = False
    normalize_None = True
    normailze_Population= False
    
    PopulationList=[]   # [code][total][men][women]
       
    In_data=[]
    Out_data=[]
    data=[]
    
    nrows=0
    In_nrows=0
    Out_nrows=0
    
    
    TotalList=[]  #8-digit Code, Points, Parts, Population, Men, Women, InCommuters, Men, Women, OutCommuters, Men, Women, Area, Name'''
    Total=True
    Men=False
    Women=False
    getInfos=False
    
    select_all=True
    select_single=False
    
    code=''
    
    HighestValue=0
    HighestBlueNet=0
    HighestRedNet=0

    
        
    
    def __init__(self,master):
        
        self.select_all=True
        self.select_single=False
        
        self.Total=True
        self.Men=False
        self.Women=False
        self.normailze_Population=False
        self.normalize_Area=False
        self.normalize_None=True
        self.In=True
        self.getInfos=False

        
        
        shpFilePathKreise="VG250_Kreise"
        shpFilePathGemeinden = "VG250_Gemeinden.shp"
        shpFilePathBundeslaender="VG250_Bundeslaender.shx"
        sf_Gemeinden=shapefile.Reader(shpFilePathGemeinden)
        self.sf_Bundeslaender=shapefile.Reader(shpFilePathBundeslaender)
        self.sf_Kreise=shapefile.Reader(shpFilePathKreise)
        
        self.KreiseShapes=self.sf_Kreise.shapes()
        self.KreiseRecords=self.sf_Kreise.records()
        
        '''
        -------------------------------------------------------------------------------------------------------------
        --------------------------create TotalList-------------------------------------------------------------------
        -------------------------------------------------------------------------------------------------------------
        '''
        '''
        with open('Out_commuter.csv', 'rb') as d:
            reader = csv.reader(d)
            self.Out_data = list(reader)
            
        with open('In_commuter.csv', 'rb') as e:
            reader = csv.reader(e)
            self.In_data = list(reader)
        
        with open('TotalList.csv', 'rb') as e:
            reader = csv.reader(e)
            TL = list(reader)
        
        
        
        '''
        workbook_your_file = xlrd.open_workbook("common_file_without_umlauts.xlsx") #change the path here if file to be read is not in the same folder
            
        In_sheet = workbook_your_file.sheet_by_index(1) #sheet number is according to einpendler or auspendler sheet
        self.In_data=[[In_sheet.cell_value(r,c)for c in range(In_sheet.ncols)] for r in range(In_sheet.nrows)]
        self.In_city_matrix = [[self.In_data[r][c]for c in range(1)] for r in range(In_sheet.nrows)]
        self.In_nrows=In_sheet.nrows
            

        
        Out_sheet = workbook_your_file.sheet_by_index(0)
    
        self.Out_data=[[Out_sheet.cell_value(r,c)for c in range(Out_sheet.ncols)] for r in range(Out_sheet.nrows)]
        self.Out_city_matrix = [[self.Out_data[r][c]for c in range(1)] for r in range(Out_sheet.nrows)]
        self.Out_nrows=Out_sheet.nrows
        

        
        file=xlrd.open_workbook('Zensus2011_Gemeinden_BRD.xls')
        sheet=file.sheet_by_index(1)
        
        for i in xrange(0,11292):
            self.PopulationList.append([])

            self.PopulationList[i].append(sheet.cell_value(i,2))
            self.PopulationList[i].append(sheet.cell_value(i,4)) 
            self.PopulationList[i].append(sheet.cell_value(i,5))
            self.PopulationList[i].append(sheet.cell_value(i,6))      

        
        
        self.ListeShapes,self.ListeRecords=GUI2HelperFunctions.getListPlaceOfAllMunicipalitiesInBavaria(sf_Gemeinden)

        for i in xrange(0,len(self.ListeRecords)):
            Code=self.ListeRecords[i][7]
            self.TotalList.append([])
            self.TotalList[i].append(self.ListeRecords[i][7])
            self.TotalList[i].append(self.ListeShapes[i].points)
            self.TotalList[i].append(self.ListeShapes[i].parts)
            for j in xrange(0,len(self.PopulationList)):
                Population=0
                M_Pop=0
                W_Pop=0
                if self.PopulationList[j][0]==self.ListeRecords[i][7]:
                    Population=self.PopulationList[j][1]
                    M_Pop=self.PopulationList[j][2]
                    W_Pop=self.PopulationList[j][3]
                    break
            self.TotalList[i].append(Population)
            self.TotalList[i].append(M_Pop)
            self.TotalList[i].append(W_Pop)
            
            
            #self.In_nrows = 147431
            for k in xrange(0,self.In_nrows):
                In_Commuters=0
                In_Commuters_M=0
                In_Commuters_W=0
                if self.In_data[k][0]==Code:
                    for j in xrange(k,self.In_nrows):
                        if self.In_data[j][2]=='Z':
                            In_Commuters=self.In_data[j][4]
                            In_Commuters_M=self.In_data[j][5]
                            In_Commuters_W=self.In_data[j][6]
                            if In_Commuters=="*":
                                In_Commuters=0
                            if In_Commuters_M=="*":
                                In_Commuters_M=0
                            if In_Commuters_W=="*":
                                In_Commuters_W=0
                            break
                    break
                    
                   
            self.TotalList[i].append(In_Commuters)
            self.TotalList[i].append(In_Commuters_M)
            self.TotalList[i].append(In_Commuters_W)
            
            
            
            #self.Out_nrows = 154718
            
            for k in xrange(0,self.Out_nrows):
                Out_Commuters=0
                Out_Commuters_M=0
                Out_Commuters_W=0
                if self.Out_data[k][0]==self.ListeRecords[i][7]:
                    for j in xrange(k,self.Out_nrows):
                        if self.Out_data[j][2]=='Z':
                            
                            Out_Commuters=self.Out_data[j][4]
                            Out_Commuters_M=self.Out_data[j][5]
                            Out_Commuters_W=self.Out_data[j][6]
                            if Out_Commuters=="*":
                                Out_Commuters=0
                            if Out_Commuters_M=="*":
                                Out_Commuters_M=0
                            if Out_Commuters_W=="*":
                                Out_Commuters_W=0
                            break
                    break
            self.TotalList[i].append(Out_Commuters)
            self.TotalList[i].append(Out_Commuters_M)
            self.TotalList[i].append(Out_Commuters_W)
        
        for j in xrange(0,len(self.TotalList)):
            poi=[]
            par=[]
            for w in xrange(0,len(self.TotalList[j][1])):
                
                poi.append(self.TotalList[j][1][w])
            par.append(self.TotalList[j][2])
         
            area=GUI2HelperFunctions.getArea(poi, par)
           
            self.TotalList[j].append(area)
        
        for i in xrange(0,len(self.ListeRecords)):
            name=self.ListeRecords[i][5]
            
            self.TotalList[i].append(name)
        
        
        '''------------------------------------------------------------------------------------------------
        --------------------write Lists--------------------------------------------------------------------
        ---------------------------------------------------------------------------------------------------
        '''
        '''
        with open("TotalList.csv", "wb") as f:
            writer = csv.writer(f)
            writer.writerows(self.TotalList)
    
        with open("In_commuter.csv", "wb") as d:
            writer=csv.writer(d)
            writer.writerows(self.In_data)
        
        with open("Out_commuter.csv", "wb") as s:
            writer=csv.writer(s)
            writer.writerows(self.Out_data)
    
        '''
        
        

        
        
        '''
        ----------------------------------------------------------------------------------------------------------------
        ------------create GUI------------------------------------------------------------------------------------------
        ----------------------------------------------------------------------------------------------------------------
        '''
            
        
         
        
        self.frame=Frame(master)
        self.frame.pack() 
       
        self.f = GUI2HelperFunctions.GetFigure(self.sf_Bundeslaender)
        DefaultSize = self.f.get_size_inches()
        self.f.set_size_inches( (DefaultSize[0]*1.3, DefaultSize[1]*1.3) )   
        self.plot = FigureCanvasTkAgg(self.f , master=self.frame)
        self.plot.get_tk_widget().grid(row=4, column = 2,columnspan=30, rowspan=30)
        
        self.Infos = Label(self.frame, text="Infomations")
        self.Infos.grid(row=4, column=32, sticky=W)
        
        self.Name = Label(self.frame, text="Name:")
        self.Name.grid(row=5, column=32,sticky=W)
        
        self.Name1=Label(self.frame, text="")
        self.Name1.grid(row=6, column=32,sticky=W)
        
        self.EinwohnerInfo = Label(self.frame, text="Total inhabitants:")
        self.EinwohnerInfo.grid(row=7, column=32, sticky=W)
        
        self.EinwohnerInfo1 = Label(self.frame, text="")
        self.EinwohnerInfo1.grid(row=8, column=32, sticky=W)
        
        self.InCommutersInfo = Label(self.frame, text="Total Incommuters:")
        self.InCommutersInfo.grid(row=9, column=32, sticky=W)
        
        self.InCommutersInfo1=Label(self.frame, text="")
        self.InCommutersInfo1.grid(row=10, column=32, sticky=W)
        
        self.OutCommutersInfo = Label(self.frame, text="Total Outcommuters:")
        self.OutCommutersInfo.grid(row=11, column=32, sticky=W)
        
        self.OutCommutersInfo1 = Label(self.frame, text="")
        self.OutCommutersInfo1.grid(row=12, column=32, sticky=W)
#########################Muhammad Mean Distance############################################3
        
        self.OutCommutersInfo = Label(self.frame, text="Mean Distance")
        self.OutCommutersInfo.grid(row=13, column=32, sticky=W)  
        
        self.OutCommutersInfo1 = Label(self.frame, text="")
        self.OutCommutersInfo1.grid(row=14, column=32, sticky=W)
        
        hello=12678
        
        self.OutCommutersInfo1 = Label(self.frame, text=hello)
        self.OutCommutersInfo1.grid(row=14, column=32, sticky=W)

##############################################################################################        
        
        
        
        toolbar_frame = Frame(self.frame)
        toolbar_frame.grid(row=3,column=0,columnspan=30, sticky=W)
        toolbar = NavigationToolbar2TkAgg( self.plot, toolbar_frame )
        
        
        str1=StringVar(self.frame)
        str1.set("Total")
        DropDown=OptionMenu(self.frame,str1,"Total", "Men", "Women",command=self.MenuGender)
        DropDown.config(width=5)
        DropDown.grid(row=0,column=0, sticky=W)
        
        str2=StringVar(self.frame)
        str2.set("In-Commuters")
        DropDown=OptionMenu(self.frame,str2,"In-Commuters", "Out-Commuters","Net", command=self.MenuCom)
        DropDown.config(width=17)
        DropDown.grid(row=0,column=1, sticky=W)
        
        
        str3=StringVar(self.frame)
        str3.set("None Normalization")
        DropDown=OptionMenu(self.frame,str3,"None Normalization", "Area Normalization", "Population Normalization",command=self.MenuNorm)
        DropDown.config(width=25)
        DropDown.grid(row=0,column=2, sticky=W)
        
            
        str5=StringVar(self.frame)
        str5.set("Off")
        DropDown=OptionMenu(self.frame,str5, "Off" , "On" , command=self.MenuInfo)
        DropDown.config(width=5)
        DropDown.grid(row=0,column=3, sticky=W)
        DropDown.grid(row=0,column=3)
        #############################################################################
<<<<<<< HEAD
#######################################COlor bar Starts################################################
        self.addColorbar(5000)                              #Calling color bar for either inflow or outflow 
        self.addColorbarNet(100)                            #Calling color bar for Netflow
        
    def addColorbar(self,H):
        
        highestValue=int(H)
        fig = pyplot.figure(figsize=(0.9,6),frameon=True,dpi=80)                    #(size length and height of the figure)
        
        fig.patch.set_facecolor('k')
        fig.patch.set_alpha(0.20)
        #fig.patch.set_visible(False)
        
=======
        ##########################COLOR BAR############################################
        '''
        fig = pyplot.figure(figsize=(4,1.5))
>>>>>>> 832ef302a1b0864e383f583068c86a8275c4f233
        
        blues=np.array(10)
        reds=np.array(10)
        reds=('#ffffff','#fee5e5','#fdcccc','#fcb2b2','#fb9999','#fb7f7f','#fa6666','#f94c4c','#f83232',(1.0, 0, 0.0))
        blues=('#ffffff','#e5e5ff','#ccccff','#b2b2ff','#9999ff','#7f7fff','#6666ff','#4c4cff','#3232ff','#0000ff')
        
        legColor=reds
        
        lowestValue=0
        highestValue=501
        noOfDivisions=10
        
        unit=(highestValue-lowestValue)/noOfDivisions
        
        legValue=(range(lowestValue, highestValue, unit))      #range(start, stop[, step])
        
        
        
        
        bounds=legValue
        
        ax = fig.add_axes([0.15, 0.05, 0.15, 0.4])            #dimension of the bar inside the figure for bar (x-coordinate, y-coordinate, length, height)
        cmap = mpl.colors.ListedColormap(legColor)
        cmap.set_over('0.25')
        cmap.set_under('0.75')
      #  bounds = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11]
        norm = mpl.colors.BoundaryNorm(bounds, cmap.N)
        cb2 = mpl.colorbar.ColorbarBase(ax, cmap=cmap,norm=norm,
                                        #boundaries=[0] + bounds + [13],
                                        #extend='both',
                                        #ticks=bounds,
                                        spacing='proportional',
                                        orientation='vertical')
        cb2.set_label('Number of commuters')
        
<<<<<<< HEAD

        self.plot = FigureCanvasTkAgg(fig , master=self.frame)
        self.plot.get_tk_widget().grid(row=4, column = 28,columnspan=10, rowspan=30,sticky=W)
        
        
        
        
        
        
        
    def addColorbarNet(self,H):
        
        highestValue=int(H)
        fig = pyplot.figure(figsize=(0.9,6),frameon=True,dpi=80)                    #(size length and height of the figure)
        
        fig.patch.set_facecolor('k')
        fig.patch.set_alpha(0.20)
        #fig.patch.set_visible(False)
        
        
        blues=np.array(10)
        reds=np.array(10)
        reds=('#ffffff','#fee5e5','#fdcccc','#fcb2b2','#fb9999','#fb7f7f','#fa6666','#f94c4c','#f83232',(1.0, 0, 0.0))
        blues=('#ffffff','#e5e5ff','#ccccff','#b2b2ff','#9999ff','#7f7fff','#6666ff','#4c4cff','#3232ff','#0000ff')
        
        
        #if self.In==True:
        legColor=reds
        
        lowestInValue=0
        highestInValue=501
        noOfDivisions=10
        
        unitIn=(highestInValue-lowestInValue)/noOfDivisions
=======
        self.f = fig
        self.plot = FigureCanvasTkAgg(self.f , master=self.frame)
        self.plot.get_tk_widget().grid(row=21, column = 12,columnspan=30, rowspan=30)
        #pyplot.show()

>>>>>>> 832ef302a1b0864e383f583068c86a8275c4f233
        
        legInValue=(range(lowestInValue, highestInValue, unitIn))      #range(start, stop[, step])
        
        
        
        
        bounds=legInValue
        
        ax1 = fig.add_axes([0.15, 0.55, 0.15, 0.4])            #dimension of the bar inside the figure for bar (x-coordinate, y-coordinate, length, height)
        cmap = mpl.colors.ListedColormap(legColor)
        cmap.set_over('0.25')
        cmap.set_under('0.75')
      #  bounds = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11]
        norm = mpl.colors.BoundaryNorm(bounds, cmap.N)
        cb2 = mpl.colorbar.ColorbarBase(ax1, cmap=cmap,norm=norm,
                                        #boundaries=[0] + bounds + [13],
                                        #extend='both',
                                        #ticks=bounds,
                                        spacing='proportional',
                                        orientation='vertical')
        cb2.set_label('Number of In commuters')
        
        ######################################Second Bar Starts#########################################
        
        
        #if self.Out==True:
        
        
        legColor=blues
        
        lowestOutValue=0
        highestOutValue=601
        noOfDivisions=10
        
        unitOut=(highestOutValue-lowestOutValue)/noOfDivisions
        
        legOutValue=(range(lowestOutValue, highestOutValue, unitOut))      #range(start, stop[, step])
        
        
        
        
        bounds=legOutValue
        
        ax2 = fig.add_axes([0.15, 0.05, 0.15, 0.4])            #dimension of the bar inside the figure for bar (x-coordinate, y-coordinate, length, height)
        cmap = mpl.colors.ListedColormap(legColor)
        cmap.set_over('0.25')
        cmap.set_under('0.75')
      #  bounds = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11]
        norm = mpl.colors.BoundaryNorm(bounds, cmap.N)
        cb2 = mpl.colorbar.ColorbarBase(ax2, cmap=cmap,norm=norm,
                                        #boundaries=[0] + bounds + [13],
                                        #extend='both',
                                        #ticks=bounds,
                                        spacing='proportional',
                                        orientation='vertical')
        cb2.set_label('Number of Out commuters')
        ######################################Second Bar Ends############################################
        
        self.plot = FigureCanvasTkAgg(fig , master=self.frame)
        self.plot.get_tk_widget().grid(row=4, column = 28,columnspan=10, rowspan=30,sticky=W)       
    
        
        ###########################################################################
        '''
        self.scrollbar=Scrollbar(self.frame)
        self.scrollbar.grid(row=4,column=0,rowspan=30, sticky=E, pady=30)
        
        self.myListbox=Listbox(self.frame, yscrollcommand = self.scrollbar.set)  
        
        myList=[]
        for i in xrange(0,len(self.TotalList)):
            if (int(self.TotalList[i][0])<10000000 and int(self.TotalList[i][0])>7999999):
                myList.append([])
                myList[i].append(self.TotalList[i][13])
            
        myList.sort()
        
        self.myListbox.insert(END, 'All')
        for i in range(0,len(myList)):
           self.myListbox.insert(END, myList[i]) 
        
        self.myListbox.grid(row=4,column=1,rowspan=30, sticky=E, pady=30)
        self.scrollbar.config(command = self.myListbox.yview)
        
        self.myListbox.bind("<Double-Button-1>", self.callback)
        

        self.f.canvas.mpl_connect('button_press_event', self.showInformations)
    
        self.plot.show()
        

    def showInformations(self, event):
        counter=0
        
        self.EinwohnerInfo1.grid_forget()
        self.Name1.grid_forget()
        self.InCommutersInfo1.grid_forget()
        self.OutCommutersInfo1.grid_forget()
        
        ix, iy = event.xdata, event.ydata
        

        if self.getInfos==True:
            for i in xrange(0,len(self.TotalList)):
            
            
                if GUI2HelperFunctions.IsPointInside(ix, iy, self.TotalList[i][1])==True:

                    counter=i
                    break
            
                
            inhab=self.TotalList[counter][3]
            name=self.TotalList[counter][13]
            Incommuter=self.TotalList[counter][6]
            Outcommuter=self.TotalList[counter][9]
        
            self.Name1=Label(self.frame, text=name)
            self.Name1.grid(row=6, column=32,sticky=W)
        
            self.EinwohnerInfo1 = Label(self.frame, text=inhab)
            self.EinwohnerInfo1.grid(row=8, column=32, sticky=W)
        
            self.InCommutersInfo1=Label(self.frame, text=Incommuter)
            self.InCommutersInfo1.grid(row=10, column=32, sticky=W)
        
            self.OutCommutersInfo1 = Label(self.frame, text=Outcommuter)
            self.OutCommutersInfo1.grid(row=12, column=32, sticky=W)
            
            
        

        

        
        
    
       
    def showMunicipality(self):
        print("Function: showMunicipality Called")


        if self.select_all==True:
            self.f, HighestValue, self.HighestRedNet, self.HighestBlueNet = GUI2HelperFunctions.getMun2(self.TotalList, self.In, self.normalize_Area, self.normalize_None, self.normailze_Population, self.Total, self.Men, self.Women, self.Out, self.Net )       
            self.plot = FigureCanvasTkAgg(self.f , master=self.frame)
            self.plot.get_tk_widget().grid(row=4, column = 2,columnspan=30,rowspan=30)#
            
            if self.Net==False:        
                self.f=GUI2HelperFunctions.addColorbar(HighestValue, self.In, self.Out)
                self.plot = FigureCanvasTkAgg(self.f , master=self.frame)
                self.plot.get_tk_widget().grid(row=15, column = 32)
            
            if self.Net==True:

                self.f=GUI2HelperFunctions.addNetColorbar(self.HighestRedNet, self.HighestBlueNet)
                self.plot = FigureCanvasTkAgg(self.f , master=self.frame)
                self.plot.get_tk_widget().grid(row=15, column = 32)
            
            
            

        
        if self.select_single==True:
            self.SingleChoice()
            if self.Net==True:
                self.f=GUI2HelperFunctions.addNetColorbar(self.HighestRedNet, self.HighestBlueNet)
                self.plot = FigureCanvasTkAgg(self.f , master=self.frame)
                self.plot.get_tk_widget().grid(row=15, column = 32)
            if self.Net==False:
                self.f=GUI2HelperFunctions.addColorbar(self.HighestValue, self.In, self.Out)
                self.plot = FigureCanvasTkAgg(self.f , master=self.frame)
                self.plot.get_tk_widget().grid(row=15, column = 32)
            
            
        
        
        for i in xrange(0,len(self.KreiseShapes)):
            
            if (int(self.KreiseRecords[i][2])<10000 and int(self.KreiseRecords[i][2])>7999):
                GUI2HelperFunctions.plotShape(self.KreiseShapes[i].points, self.KreiseShapes[i].parts,'',PlotColor='k')    
        
        
        self.f.canvas.mpl_connect('button_press_event', self.showInformations)
        self.plot.show()
        
        toolbar_frame = Frame(self.frame) 
        toolbar_frame.grid(row=3,columnspan=30,sticky=W) 
        toolbar = NavigationToolbar2TkAgg( self.plot, toolbar_frame )
        
        
    '''  
    def PopupSelect(self):
        top = Toplevel()
        top.title("About this application...")

        msg = Message(top, text='Klick Municipality')
        msg.pack()

        button = Button(top, text="OK", command=top.destroy)
        button.pack()
        
        self.f.canvas.mpl_connect('button_press_event', self.CallSingleChoice)
        self.plot.show()
        

        
    def CallSingleChoice(self,event):
        ix, iy = event.xdata, event.ydata
        
        
        for i in xrange(0,len(self.TotalList)):
            
            if GUI2HelperFunctions.IsPointInside(ix, iy, self.TotalList[i][1])==True:

                    counter=i
                    break
        self.SingleChoice(self.TotalList[counter][0])
    '''    
         
    def SingleChoice(self):
        code=self.code
        #Code, Points, Parts, Population, Men, Women, InCommuters, Men, Women, OutCommuters, Men, Women, Area, Name'''
        #  0     1         2      3        4     5        6          7     8     9             10  11     12    13
        #code='08111000'
        PlaceInList=0
        '''
        get 8-digit-Code of chosen city
        '''
        '''
        plot whole card white
        '''
        self.f = GUI2HelperFunctions.GetFigure(self.sf_Bundeslaender, FillColor='silver')
        self.plot = FigureCanvasTkAgg(self.f , master=self.frame)
        self.plot.get_tk_widget().grid(row=4, column = 2,columnspan=30, rowspan=30)
        
        '''
        mark chosen Mun in green Color
        '''
        for i in xrange(0,len(self.TotalList)):
            if self.TotalList[i][0]==code:
                PlaceInList=i
                break
        
        GUI2HelperFunctions.plotShape(self.TotalList[PlaceInList][1], self.TotalList[PlaceInList][2], 'g')
        
        if self.Net==True:
            '''
            create List with all commuterMunicipalities   
            '''
            counter=0
            ComMuni=[]
            
            for i in xrange(0,len(self.In_data)):
                if self.In_data[i][0]==code:
                    for j in xrange(i+1,len(self.In_data)):
                        if self.In_data[j][2].isdigit()==True:
                                
                                if (int(self.In_data[j][2])<10000000 and int(self.In_data[j][2])>7999999):
                                    
                                    
                                    ComMuni.append([])
                                    ComMuni[counter].append(self.In_data[j][2])
                                
                                
                                    if self.Total==True:
                                        ComMuni[counter].append(self.In_data[j][4])
                                    if self.Men==True:
                                        ComMuni[counter].append(self.In_data[j][5])
                                    if self.Women==True:
                                        ComMuni[counter].append(self.In_data[j][6])
                                        
                                    counter=counter+1
                            
                        elif self.In_data[j][2].isdigit()==False:
                            break
                        
                    break
            '''
            until now: Communi:  code/Incom
            now: Communi: code/IncomOutcom
            '''
            for i in xrange(0,len(self.Out_data)):
                if self.Out_data[i][0]==code:
                    for j in xrange(i+1,len(self.Out_data)):
                        if self.Out_data[j][2].isdigit()==True:
                            
                            if (int(self.Out_data[j][2])<10000000 and int(self.Out_data[j][2])>7999999):
                                '''
                                check if code is already in the list
                                '''
                                d=0
                                for l in xrange(0,len(ComMuni)):
                                    d=d+1
                                    if ComMuni[l][0]==self.Out_data[j][2]:
                                        d=0
                                        if self.Total==True:
                                            ComMuni[l].append(self.Out_data[j][4])
                                        if self.Men==True:
                                            ComMuni[l].append(self.Out_data[j][5])
                                        if self.Women==True:
                                            ComMuni[l].append(self.Out_data[j][6])
                                        break
                                if d==len(ComMuni):
                                    ComMuni.append([])
                                    ComMuni[len(ComMuni)-1].append(self.Out_data[j][2])
                                    ComMuni[len(ComMuni)-1].append(0)
                                    if self.Total==True:
                                        ComMuni[len(ComMuni)-1].append(self.Out_data[j][4])
                                    if self.Men==True:
                                        ComMuni[len(ComMuni)-1].append(self.Out_data[j][5])
                                    if self.Women==True:
                                        ComMuni[len(ComMuni)-1].append(self.Out_data[j][6])
                        if self.Out_data[j][2].isdigit()==False:    
                            break       
                    
                    break
            '''
            add 0 Outcommuters if necessary
            '''
                        
                        
            for i in xrange(0,len(ComMuni)):
                if len(ComMuni[i])==2:
                    ComMuni[i].append(0)
            
            '''
            compute dif. :  incom - outcom:  => more incommuter: positiv
                                            => more outcommuter: negativ
            '''
            for i in xrange(0,len(ComMuni)):
                if (ComMuni[i][1]=='*' or ComMuni[i][1]=='-'):
                    ComMuni[i][1]=0
                if (ComMuni[i][2]=='*' or ComMuni[i][2]=='-'):
                    ComMuni[i][2]=0
                dif= float(ComMuni[i][1])-float(ComMuni[i][2])
                ComMuni[i].append(dif)
            
            '''
            compute density and add
            '''
            
            if self.normalize_None==True:
                for i in xrange(0,len(ComMuni)):
                    density=ComMuni[i][3]
                    ComMuni[i].append(density)
                    
            if self.normalize_Area==True:
                
                for i in xrange(0, len(ComMuni)):
                    density=0
                    #get Area
                    for j in xrange(0,len(self.TotalList)):
                        if ComMuni[i][0]==self.TotalList[j][0]:
                            area=self.TotalList[j][12]
                            break
                    if (area==0 or area=='*' or area=='-'):
                        density=0
                    else:
                        density=float(ComMuni[i][3])/float(area)
                    ComMuni[i].append(density)
            
            if self.normailze_Population==True:
                
                for i in xrange(0,len(ComMuni)):
                    density=0
                    popul=0
                    #get Population
                    for j in xrange(0,len(self.TotalList)):
                        if ComMuni[i][0]==self.TotalList[j][0]:
                            if self.Total==True:
                                popul=self.TotalList[j][3]
                            if self.Men==True:
                                popul=self.TotalList[j][4]
                            if self.Women==True:
                                popul=self.TotalList[j][5]
                            break
                    if (popul==0 or popul=='*' or popul=='-'):
                        density=0
                    else:
                        density=float(ComMuni[i][3])/float(popul)
                    
                    ComMuni[i].append(density)
            
            '''
            compute color
            '''
            for i in xrange(0,len(ComMuni)):
                color='silver'
                counterSmaller=0
                counterEnt=0
                procentual=0
                if ComMuni[i][4]>0:
                    for j in xrange(0,len(ComMuni)):
                        if ComMuni[j][4]>0:
                            counterEnt=counterEnt+1
                        if (ComMuni[j][4]>0 and ComMuni[j][4]<=ComMuni[i][4]):
                            counterSmaller=counterSmaller+1
                    procentual=float(counterSmaller)/float(counterEnt)
                
                    color=GUI2HelperFunctions.getColor2(procentual, True, False)
                if ComMuni[i][4]<0:
                    for j in xrange(0,len(ComMuni)):
                        if ComMuni[j][4]<0:
                            counterEnt=counterEnt+1
                        if (ComMuni[j][4]<0 and ComMuni[j][4]>=ComMuni[i][4]):
                            counterSmaller=counterSmaller+1
                    procentual=float(counterSmaller)/float(counterEnt)
                    
                    color=GUI2HelperFunctions.getColor2(procentual, False, True)
                ComMuni[i].append(color)
            
            '''
            plot
            '''
            for i in xrange(0,len(ComMuni)):
                for j in xrange(0,len(self.TotalList)):
                    if self.TotalList[j][0]==ComMuni[i][0]:
                        
                        GUI2HelperFunctions.plotShape(self.TotalList[j][1], self.TotalList[j][2], ComMuni[i][5])
                        break    
                    
                
            
            # code in out diff densit color
            self.HighestBlueNet=0
            self.HighestRedNet=0
            for i in xrange(0,len(ComMuni)):
                if ComMuni[i][4]<0:
                    if ComMuni[i][4]<self.HighestBlueNet:
                        self.HighestBlueNet=ComMuni[i][4]
                if ComMuni[i][4]>0:
                    if ComMuni[i][4]>self.HighestRedNet:
                        self.HighestRedNet=ComMuni[i][4]
            
            self.HighestBlueNet=int(float(self.HighestRedNet)*float(-1))
            
                    
            '''
            for i in xrange(0,len(ComMuni)):
                print(ComMuni[i][:])
            for i in xrange(0,len(ComMuni)):
                if len(ComMuni[i])!=6:
                    print('error')
            '''
        
        if self.Net==False:
            '''
            create List with all commuterMunicipalities   
            '''
            
            '''Inncommuters'''
            counter=0
            ComMuni=[]    
            
            if self.In==True:
                
                for i in xrange(0,len(self.In_data)):
     
                    if self.In_data[i][0]==code:
                       
                        
                        
                        for j in xrange(i+1,len(self.In_data)):
                            
                            
                            if self.In_data[j][2].isdigit()==True:
                                
                                if (int(self.In_data[j][2])<10000000 and int(self.In_data[j][2])>7999999):
                                    
                                    
                                    ComMuni.append([])
                                    ComMuni[counter].append(self.In_data[j][2])
                                
                                
                                    if self.Total==True:
                                        ComMuni[counter].append(self.In_data[j][4])
                                    if self.Men==True:
                                        ComMuni[counter].append(self.In_data[j][5])
                                    if self.Women==True:
                                        ComMuni[counter].append(self.In_data[j][6])
                                    
                                
                                    counter=counter+1
                            
                            elif self.In_data[j][2].isdigit()==False:
                                break
                        
                        break
            
            '''
            Outcommuters
            '''
            if self.Out==True:
                
                for i in xrange(0,len(self.Out_data)):
                    
                    if self.Out_data[i][0]==code:
                       
                        
                        
                        for j in xrange(i+1,len(self.Out_data)):
                            
                            
                            if self.Out_data[j][2].isdigit()==True:
                                
                                if (int(self.Out_data[j][2])<10000000 and int(self.Out_data[j][2])>7999999):
                                    
                                    
                                    ComMuni.append([])
                                    ComMuni[counter].append(self.Out_data[j][2])
                                
                                
                                    if self.Total==True:
                                        ComMuni[counter].append(self.Out_data[j][4])
                                    if self.Men==True:
                                        ComMuni[counter].append(self.Out_data[j][5])
                                    if self.Women==True:
                                        ComMuni[counter].append(self.Out_data[j][6])
                                    
                                
                                    counter=counter+1
                            
                            elif self.Out_data[j][2].isdigit()==False:
                                break
                        
                        break
            
    
                    
            '''
            With Normalization
           '''
            if self.normalize_None==True:
                for i in xrange(0,len(ComMuni)):
                    ComMuni[i].append(ComMuni[i][1])
                    
                    
            elif self.normalize_None==False:
                
                for i in xrange(0,len(self.TotalList)):
                    density=0
                    
                    for j in xrange(0,len(ComMuni)):
                        if self.TotalList[i][0]==ComMuni[j][0]:
                
                            if self.normalize_Area==True:
                                area=self.TotalList[i][12]
                                
                                if area==0 or area=='*' or area=='-' or ComMuni[j][1]==0 or ComMuni[j][1]=='*' or ComMuni[j][1]=='-':
                                    density=0
                                else:
                                    density=float(ComMuni[j][1])/float(area)
                                    
                            
                            if self.normailze_Population==True:
                                population=0
                                if self.Total==True:
                                    population=self.TotalList[i][3]
                                if self.Men==True:
                                    population=self.TotalList[i][4]
                                if self.Women==True:
                                    population=self.TotalList[i][5]
                                    
                                 
                                
                                if population==0 or population=='*' or population=='-' or ComMuni[j][1]==0 or ComMuni[j][1]=='*' or ComMuni[j][1]=='-' :
                                    density=0
                                else:
                                
                                    density=float(ComMuni[j][1])/float(population)
                                    
                                
                    
                    
                            ComMuni[j].append(density)
    
            
                
            '''
            get Colors
            '''
                            
    
            
            
            for j in xrange(0,len(ComMuni)):
                x=ComMuni[j][2]
                smaller=0
                for i in xrange(0,len(ComMuni)):
                    if ComMuni[i][2]<=x:
                        smaller=smaller+1
            
                procentual=float(smaller)/float(len(ComMuni))
                
                color=GUI2HelperFunctions.getColor2(procentual, self.In, self.Out)
        
                ComMuni[j].append(color)
                
            
            '''
            ------------------------------------------
            plot shapes
            ------------------------------------------
            '''
    
            for i in xrange(0,len(ComMuni)):
                
                for j in xrange(0,len(self.TotalList)):
                    if ComMuni[i][0]==self.TotalList[j][0]:
                        GUI2HelperFunctions.plotShape(self.TotalList[j][1], self.TotalList[j][2], ComMuni[i][len(ComMuni[i])-1])
                        break
                    
            self.HighestValue=0
            for i in xrange(0,len(ComMuni)):
                if ComMuni[i][2]>self.HighestValue:
                    self.HighestValue=ComMuni[i][2]
            

                
                
                
                
    
    
            '''
            append Length
                   
            for i in xrange(0,len(ComMuni)):
                Length=0
                Length=getLength(code,ComMuni[i][0])
                
                ComMuni[i].append(Length)
                
            '''
                
            #x,y=self.getCenter(code)
            #print(x,y)
            
                    
             
            
    
        '''
    def InCommuters(self):
        print("Function: InCommuters Called")
        
        
        
        self.In=True
        if self.county==True:
            self.showCounties()
        if self.county==False:
            self.showMunicipality()
    
    def OutCommuters(self):
        print("Function: OutCommuters Called")
        
        self.city_matrix=self.Out_city_matrix
        self.data=self.Out_data
        self.nrows=self.Out_nrows
        
        self.In=False
        if self.county==True:
            self.showCounties()
        if self.county==False:
            self.showMunicipality()
    '''

    def NoneNormalization(self):
        self.normalize_None=True
        self.normalize_Area=False
        self.normailze_Population=False
        
        if self.county==True:
            self.showCounties()
        if self.county==False:
            self.showMunicipality()
        
    def AreaNormalization(self):
        self.normalize_None=False
        self.normalize_Area=True
        self.normailze_Population=False
        
        if self.county==True:
            self.showCounties()
        if self.county==False:
            self.showMunicipality()
   
    def PopulationNormalization(self):
        self.normalize_None=False
        self.normalize_Area=False
        self.normailze_Population=True
        
        if self.county==True:
            self.showCounties()
        if self.county==False:
            self.showMunicipality()
        
    def TotalFunction(self):
        self.Men=False
        self.Women=False
        self.Total=True
        
        if self.county==True:
            self.showCounties()
        if self.county==False:
            self.showMunicipality()
    
    def MenFunction(self):
        self.Men=True
        self.Women=False
        self.Total=False
        
        if self.county==True:
            self.showCounties()
        if self.county==False:
            self.showMunicipality()
    
    def WomenFunction(self):
        self.Men=False
        self.Women=True
        self.Total=False
        
        if self.county==True:
            self.showCounties()
        if self.county==False:
            self.showMunicipality()
     
    def MenuGender(self,value):
        if value=="Men":
            self.MenFunction()
        elif value=="Women":
            self.WomenFunction()
        elif value=="Total":
            self.TotalFunction()
    
    def MenuCom(self,value):
        
        if value=="In-Commuters":
            self.In=True
            self.Net=False
            self.Out=False
        elif value=="Out-Commuters":
            self.In=False
            self.Out=True
            self.Net=False
        elif value=="Net":
            self.In=False
            self.Out=False
            self.Net=True
        
        self.showMunicipality()
            
    
    def MenuNorm(self,value):
        
        if value=="None Normalization":
            self.NoneNormalization()
        elif value=="Area Normalization":
            self.AreaNormalization()
        elif value=="Population Normalization":
            self.PopulationNormalization()
            
    def MenuSelect(self,value):
        

        
        if value=="All":
            self.select_all=True
            self.select_single=False
        else:
            self.select_all=False
            self.select_single=True
            
        
        
        self.showMunicipality()
            

        
     
    def MenuInfo(self, value):
        if value == "On" :
            self.getInfos=True
        elif value == "Off" :
            self.getInfos=False
    
    def callback(self,event):
        widget = event.widget
        selection=widget.curselection()
        
        value = widget.get(selection[0])            #mein jisko dabata hun uski name value me sama jaata hai
        
        if value!="All":
            for i in xrange(0,len(self.TotalList)):
                
                newString="['" +  self.TotalList[i][13] + "']"
                
                if value==newString:
                    
                    self.code=self.TotalList[i][0]
                    break
        
        self.MenuSelect(value)
    
    def getCenter(self,code):
        for i in xrange(0,len(self.ListeRecords)):
            if self.ListeRecords[i][7]==code:
                shape=self.ListeShapes[i].bbox
                x1=shape[0]
                x2=shape[2]
                y1=shape[1]
                y2=shape[3]
                
                x=x1+(x2-x1)/2
                y=y1+(y2-y1)/2
                
        return x,y
    
    
    def addColorbar(self,H):
        highestValue=int(H)
        fig = pyplot.figure(figsize=(1.0,3),frameon=True,dpi=80)                    #(size length and height of the figure)
        
        fig.patch.set_facecolor('r')
        fig.patch.set_alpha(0.0)
        #fig.patch.set_visible(False)
        
        
        blues=np.array(10)
        reds=np.array(10)
        reds=('#ffffff','#fee5e5','#fdcccc','#fcb2b2','#fb9999','#fb7f7f','#fa6666','#f94c4c','#f83232',(1.0, 0, 0.0))
        blues=('#ffffff','#e5e5ff','#ccccff','#b2b2ff','#9999ff','#7f7fff','#6666ff','#4c4cff','#3232ff','#0000ff')
        
        
        if self.In==True:
            legColor=reds
        if self.Out==True:
            legColor=blues
        
        
        lowestValue=0
       
        noOfDivisions=10
        
        unit=(highestValue-lowestValue)/noOfDivisions
        
        legValue=(range(lowestValue, highestValue, unit))      #range(start, stop[, step])
        
        
        
        
        bounds=legValue
        
        ax2 = fig.add_axes([0.15, 0.05, 0.15, 0.9])            #dimension of the bar inside the figure for bar (x-coordinate, y-coordinate, length, height)
        cmap = mpl.colors.ListedColormap(legColor)
        cmap.set_over('0.25')
        cmap.set_under('0.75')
      #  bounds = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11]
        norm = mpl.colors.BoundaryNorm(bounds, cmap.N)
        cb2 = mpl.colorbar.ColorbarBase(ax2, cmap=cmap,norm=norm,
                                        #boundaries=[0] + bounds + [13],
                                        #extend='both',
                                        #ticks=bounds,
                                        spacing='proportional',
                                        orientation='vertical')
        cb2.set_label('Number of commuters')
        

        
        
        return fig

        

        



    
        

root= Tk()
a = KreisePage(root)
root.mainloop()
<<<<<<< HEAD

=======
#pyplot.show()
>>>>>>> 832ef302a1b0864e383f583068c86a8275c4f233
