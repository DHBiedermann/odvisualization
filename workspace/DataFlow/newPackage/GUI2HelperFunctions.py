#!/usr/bin/python
# -*- coding: iso-8859-15 -*-
import os, sys
from newPackage import ShapefileReader
from newPackage import shapefile
import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from newPackage.ShapefileReader import GetFigure
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg, NavigationToolbar2TkAgg
#from Tkinter import *
from newPackage import total_in_commuters
from newPackage import total_out_commuters
from newPackage import returnList
from math import sqrt
from matplotlib import pyplot








def getMun(TotalList,In, AreaNom, NoneNome, PopulationNom, Total, Men, Women): 
  
    for i in xrange(0,len(TotalList)):    #for every municipality         
        Population=0
        color=''
        commuters=0
        
        
            
            
        if In==True:
            if Total==True:
                commuters=TotalList[i][6]
            elif Men==True:
                commuters=TotalList[i][7]
            elif Women==True:
                commuters=TotalList[i][8]
        elif In==False:
            if Total==True:
                commuters=TotalList[i][9]
            elif Men==True:
                commuters=TotalList[i][10]
            elif Women==True:
                commuters=TotalList[i][11]
    
    
        if AreaNom==True:
            area=TotalList[i][12]
            if area==0:
                densitiy=0
            else:    
                density=commuters/area
                
        elif PopulationNom==True:
            if Total==True:
                Population=TotalList[i][3]
            elif Men==True:
                Population=TotalList[i][4]
            elif Women==True:
                Population=TotalList[i][5]
                
                
            if Population==0:
                density=0
            else:
                density=commuters/Population
                    
                
                
               
        elif NoneNome==True:
            density=commuters
                        
        
        color=getColor(density, In, NoneNome, AreaNom, PopulationNom, Total, Men, Women)
        

        plotShape(TotalList[i][1], TotalList[i][2], color)        
        
          
               
    fig=plt.figure(1)
    
    
    
    
    return fig


def getListPlaceOfAllMunicipalitiesInBavaria(YYY):
    ListeShapes=[]
    ListeRecords=[]
    
    
    


    
    records=YYY.records()
    for i in xrange(0,len(records)):
        if (int(records[i][7])<10000000 and int(records[i][7])>7999999):
    
            ListeShapes.append(YYY.shape(i))
            ListeRecords.append(YYY.record(i))
            
        
    
    return ListeShapes, ListeRecords


def getColor(density,In,NoneNom,AreaNom,PopNom,Total,Men,Women):
    
    
    max=0
    
    
    if In==True:
        if Total==True:
            if NoneNom==True:
                max=348855
            if AreaNom==True:
                max=3.5*823418.815608
                #max=2.012568199532346
            if PopNom==True:
                max=2.012568199532346
        if Men==True:
            if NoneNom==True:
                max=196209
            if AreaNom==True:
                max=3.5*449871.23138
                #max=1.6628694460755527
            if PopNom==True:
                max=2.2628694460755527
        if Women==True:
            if NoneNom==True:
                max=152646
            if AreaNom==True:
                max=3.5*376558.992155
                #max=1.3948998178506374
            if PopNom==True:
                max=2.1948998178506374
                
    if In==False:
        if Total==True:
            if NoneNom==True:
                max=154345
            elif AreaNom==True:
                max=3.5*991102.290685
                #max=0.503903200624512
            elif PopNom==True:
                max=0.5039032006245121    
        elif Men==True:
            if NoneNom==True:
                max=92323
            elif AreaNom==True:
                max=3.5*541027.107453
                #max=0.3268656716417911
            elif PopNom==True:
                max=0.6268656716417911
        elif Women==True:
            if NoneNom==True:
                max=62022
            elif AreaNom==True:
                max=3.5*450075.183232
                #max=0.28300395256916995
            elif PopNom==True:
                max=0.48300395256916995
    
         
    
    
    if In==True:
        '''
        if density==0:
            color="silver"
        elif density==max:
            color='#ff0000'
        else:
            x=int(9999-((density/max)*9999))
            color="#ff%s" %(x)
        ''' 
        
        if (density>(0.0*max) and density<=(0.05*max)): #IN Municipalite Bavaria #change to incommuters
            color='#ffffff'
        elif (density > (0.05*max) and density <= (0.1*max)):
            color = '#fee5e5'
        elif (density > (0.1*max) and density <= (0.15*max)):
            color = '#fdcccc'
        elif (density > (0.15*max) and density <= (0.2*max)):
            color = '#fcb2b2'
        elif (density > (0.2*max) and density <= (0.25*max)):
            color = '#fb9999'
        elif (density > (0.25*max) and density <= (0.3*max)):
            color = '#fb7f7f'
        elif (density > (0.3*max) and density <= (0.35*max)):
            color = '#fa6666'
        elif (density > (0.35*max) and density <= (0.4*max)):
            color = '#f94c4c'
        elif (density > (0.4*max) and density <= (0.45*max)):
            color = '#f83232'
        elif density > (0.45*max):
            color = (1.0, 0, 0.0)
        else :
            color='silver'
            
            
        
    elif In==False:
        if (density>(0.0*max) and density<=(0.1*max)):     #out municipality Badenwutternberg
            color='#ffffff'
        elif (density>(0.1*max) and density<=(0.2*max)):
            color='#e5e5ff'
        elif (density>(0.2*max) and density<=(0.3*max)):
            color='#ccccff'
        elif (density>(0.3*max) and density<=(0.4*max)):
            color='#b2b2ff'
        elif (density>(0.4*max) and density<=(0.5*max)):
            color='#9999ff'
        elif (density>(0.5*max) and density<=(0.6*max)):
            color='#7f7fff'
        elif (density>(0.6*max) and density<=(0.7*max)):
            color='#6666ff'
        elif (density>(0.7*max) and density<=(0.8*max)):
            color='#4c4cff'
        elif (density>(0.8*max) and density<=(0.9*max)):
            color='#3232ff'
        elif (density>0.9):
            color='#0000ff'
        else :
            color='silver'
    
        

   
    return color

def getArea(PointList, PartList):
    

    
    
    
    TwoArea=0
    if len(PartList)==1:
        PointList.append(PointList[0])
        PointList.append(PointList[1])
        for i in xrange(1,len(PointList)-1):
            
            x_i = PointList[i][0]
            y_i0 = PointList[i-1][1]
            y_i2 = PointList[i+1][1]
            TwoArea = TwoArea + x_i*(y_i2 - y_i0)
    
    elif len(PartList)!=1:
        PartList.append(len(PointList))
        for i in xrange(0, len(PartList)-1):
            NewPointList=[]
            for j in xrange(PartList[i], PartList[i+1]):
                NewPointList.append(PointList[j])
                
            NewPointList.append(NewPointList[0])
            NewPointList.append(NewPointList[1])
            
            for k in xrange(1,len(PointList)-1):
            
                x_i = PointList[k][0]
                y_i0 = PointList[k-1][1]
                y_i2 = PointList[k+1][1]
                
                TwoArea = TwoArea + x_i*(y_i2 - y_i0)
                

    return sqrt(TwoArea*TwoArea)*0.5

def plotShape(PointList, PartList, FillColor, PlotColor=""):

    if len(PartList)==1:
        X=[]
        Y=[]
        for r in xrange(0,len(PointList)):
            X.append(PointList[r][0]) 
            Y.append(PointList[r][1])
        if FillColor!="":
            plt.fill(X,Y,color=FillColor)
            
        
        if PlotColor!="":
            plt.plot(X,Y,color=PlotColor)
    
    elif len(PartList)!=1:
            parts=len(PartList)
            for j in xrange(0,parts-1):
                X=[]
                Y=[]
                for k in xrange(PartList[j] , PartList[j+1] ):
                    X.append(PointList[k][0]) 
                    Y.append(PointList[k][1])
                if FillColor!="":
                    plt.fill(X,Y,color=FillColor)
                if PlotColor!="":
                    plt.plot(X,Y,color=PlotColor)
                
            X=[]
            Y=[]
            for kk in xrange(PartList[parts-1],len(PointList)):
                X.append(PointList[kk][0]) 
                Y.append(PointList[kk][1])
            if len(X)!=1:
                if FillColor!="":
                    plt.fill(X,Y,color=FillColor)
            if PlotColor!="":
                plt.plot(X,Y,color=PlotColor)
                         
            
    return True

def IsPointInside(x,y,poly):
        n = len(poly)
        inside = False

        p1x,p1y = poly[0]
        for i in range(n+1):
            p2x,p2y = poly[i % n]
            if y > min(p1y,p2y):
                if y <= max(p1y,p2y):
                    if x <= max(p1x,p2x):
                        if p1y != p2y:
                            xints = (y-p1y)*(p2x-p1x)/(p2y-p1y)+p1x
                        if p1x == p2x or x <= xints:
                            inside = not inside
            p1x,p1y = p2x,p2y

        return inside

def GetFigure(test, FillColor='g'):
    shapes=test.shapes()
    
    for State in (5,11,19):   # for all shapes in the shapefile
        plotShape(shapes[State].points, shapes[State].parts, FillColor,'k')

    fig=plt.figure(1)

    
    return fig    

def getColor2(procentual,In,Out):
    
    if In==True:
                        
        if (procentual>0 and procentual<=0.1):
            color='#ffffff'
        elif procentual<=0.2:
            color = '#fee5e5'
        elif procentual<=0.3:
            color = '#fdcccc'
        elif procentual<=0.4:
            color = '#fcb2b2'
        elif procentual<=0.5:
            color = '#fb9999'
        elif procentual<=0.6:
            color = '#fb7f7f'
        elif procentual<=0.7:
            color = '#fa6666'
        elif procentual<=0.8:
            color = '#f94c4c'
        elif procentual<=0.9:
            color = '#f83232'
        elif procentual<=1.0:
            color = (1.0, 0, 0.0)
        else:
            color='silver'

            
    elif Out==True:
        
        if (procentual>0 and procentual<=0.1):
            color='#ffffff'
        elif procentual<=0.2:
            color='#e5e5ff'
        elif procentual<=0.3:
            color='#ccccff'
        elif procentual<=0.4:
            color='#b2b2ff'
        elif procentual<=0.5:
            color='#9999ff'
        elif procentual<=0.6:
            color='#7f7fff'
        elif procentual<=0.7:
            color='#6666ff'
        elif procentual<=0.8:
            color='#4c4cff'
        elif procentual<=0.9:
            color='#3232ff'
        elif procentual<=1.0:
            color='#0000ff'
        else:
            color='silver'
    
    
    

   
    return color

def getMun2(TotalList,In, AreaNom, NoneNome, PopulationNom, Total, Men, Women, Out, Net): 
  
    densityList=[]
    HighestValue=0
    HBlue=0
    HRed=0
    
    
    if Net==False:
        for i in xrange(0,len(TotalList)):    #for every municipality         
            Population=0
            color=''
            commuters=0
    
            
            
                
                
            if In==True:
                if Total==True:
                    commuters=TotalList[i][6]
                elif Men==True:
                    commuters=TotalList[i][7]
                elif Women==True:
                    commuters=TotalList[i][8]
            elif Out==True:
                if Total==True:
                    commuters=TotalList[i][9]
                elif Men==True:
                    commuters=TotalList[i][10]
                elif Women==True:
                    commuters=TotalList[i][11]
        
        
            if AreaNom==True:
                area=TotalList[i][12]
                if area==0:
                    density=0
                else:    
                    density=commuters/area
                    
            elif PopulationNom==True:
                if Total==True:
                    Population=TotalList[i][3]
                elif Men==True:
                    Population=TotalList[i][4]
                elif Women==True:
                    Population=TotalList[i][5]
                    
                    
                if Population==0:
                    density=0
                else:
                    density=commuters/Population
                        
                    
                    
                   
            elif NoneNome==True:
                density=commuters
                            
            densityList.append([])
            densityList[i].append(density)
         
    
        for i in xrange(0,len(densityList)):
            smaller=0
            procentual=0
            for j in xrange(0,len(densityList)):
                if densityList[j]<=densityList[i]:
                    smaller=smaller+1
            procentual=float(smaller)/float(len(densityList))
            color=getColor2(procentual, In, Out)
            plotShape(TotalList[i][1], TotalList[i][2], color)
        
        for i in xrange(0,len(densityList)):
            if densityList[i][0]>HighestValue:
                HighestValue=densityList[i][0]
            
      
                       
        
    
    if Net==True:
        for i in xrange(0, len(TotalList)):
            OutCom=0
            InCom=0
            density=0                   
            Population=0
            densityList.append([])      # densityList: InCom/OutCom/Differenz/density/color
                                        # If density>0: more Incommuter: red
                                        # If density<0: more Outcommuter: blue
        
            # add Incommuters
            
            if Total==True:
                InCom=TotalList[i][6]
            elif Men==True:
                InCom=TotalList[i][7]
            elif Women==True:
                InCom=TotalList[i][8]
            
            densityList[i].append(InCom)
            
            
            if Total==True:
                OutCom=TotalList[i][9]
            elif Men==True:
                OutCom=TotalList[i][10]
            elif Women==True:
                OutCom=TotalList[i][11]
            
            densityList[i].append(OutCom)
            
            dif= float(InCom)-float(OutCom)
            densityList[i].append(dif)
            
            if NoneNome==True:
                density=densityList[i][2]
            
            elif PopulationNom==True:
            
                if Total==True:
                    Population=TotalList[i][3]
                elif Men==True:
                    Population=TotalList[i][4]
                elif Women==True:
                    Population=TotalList[i][5]

                if float(Population)==0:
                    density=0
                else:
                    density=float(densityList[i][2])/float(Population)
                
            elif AreaNom==True:
                area=TotalList[i][12]
                if float(area)==0:
                    density=0
                else:    
                    density=float(densityList[i][2])/float(area)
                    
            
            
            densityList[i].append(density)

        for i in xrange(0,len(densityList)):
            color='silver'
            counterSmaller=0
            counterEnt=0
            procentual=0
            if densityList[i][3]>0:
                for j in xrange(0,len(densityList)):
                    if densityList[j][3]>0:
                        counterEnt=counterEnt+1
                    if (densityList[j][3]>0 and densityList[j][3]<=densityList[i][3]):
                        counterSmaller=counterSmaller+1
                procentual=float(counterSmaller)/float(counterEnt)
                
                color=getColor2(procentual, True, False)
            if densityList[i][3]<0:
                for j in xrange(0,len(densityList)):
                    if densityList[j][3]<0:
                        counterEnt=counterEnt+1
                    if (densityList[j][3]<0 and densityList[j][3]>=densityList[i][3]):
                        counterSmaller=counterSmaller+1
                procentual=float(counterSmaller)/float(counterEnt)
                    
                color=getColor2(procentual, False, True)
            densityList[i].append(color)
        
        for i in xrange(0,len(densityList)):
            plotShape(TotalList[i][1], TotalList[i][2], densityList[i][4])
        
        HBlue=0
        HRed=0
         
        for i in xrange(0,len(densityList)):
            if densityList[i][3]<0:
                if densityList[i][3]<HBlue:
                    HBlue=densityList[i][3]
            if densityList[i][3]>0:
                if densityList[i][3]>HRed:
                    HRed=densityList[i][3]
            
         
         
        '''  
        for i in xrange(0,len(densityList)):
            print(densityList[i][:])
        for i in xrange(0,len(densityList)):
            if len(densityList[i])!=5:
                print('error')    
            
        '''
        '''
        '''     
    fig=plt.figure(1)
    
    
    return fig, HighestValue, HRed, HBlue

def addColorbar(H, In , Out):
    d=True
    if d==True:
        highestValue=int(H)+1
        fig = pyplot.figure(figsize=(1.0,3),frameon=True,dpi=80)                    #(size length and height of the figure)
        
        fig.patch.set_facecolor('r')
        fig.patch.set_alpha(0.0)
        #fig.patch.set_visible(False)
        
        
        blues=np.array(10)
        reds=np.array(10)
        reds=('#ffffff','#fee5e5','#fdcccc','#fcb2b2','#fb9999','#fb7f7f','#fa6666','#f94c4c','#f83232',(1.0, 0, 0.0))
        blues=('#ffffff','#e5e5ff','#ccccff','#b2b2ff','#9999ff','#7f7fff','#6666ff','#4c4cff','#3232ff','#0000ff')
        
        
        if In==True:
            legColor=reds
        if Out==True:
            legColor=blues
        
        
#         lowestValue=0
#        
#         noOfDivisions=10
#         
#         unit=(highestValue-lowestValue)/noOfDivisions
#     
#         
#         legValue=(range(lowestValue, highestValue, unit))      #range(start, stop[, step])
        
        
        mean=highestValue/2
        legValue=[0,mean, highestValue]
        bounds=legValue
        
        ax2 = fig.add_axes([0.15, 0.05, 0.15, 0.9])            #dimension of the bar inside the figure for bar (x-coordinate, y-coordinate, length, height)
        cmap = mpl.colors.ListedColormap(legColor)
        cmap.set_over('0.25')
        cmap.set_under('0.75')
      #  bounds = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11]
        norm = mpl.colors.BoundaryNorm(bounds, cmap.N)
        cb2 = mpl.colorbar.ColorbarBase(ax2, cmap=cmap,norm=norm,
                                        #boundaries=[0] + bounds + [13],
                                        #extend='both',
                                        #ticks=bounds,
                                        spacing='proportional',
                                        orientation='vertical')
        cb2.set_label('Number of commuters')
        

        

        
        return fig
    
def addNetColorbar(HRed,HBlue):
    d=True
    if d==True:
        ###Red##################################################
        fig = pyplot.figure(figsize=(1.0,3),frameon=True,dpi=80)                    #(size length and height of the figure)
        
        fig.patch.set_facecolor('r')
        fig.patch.set_alpha(0.0)
        #fig.patch.set_visible(False)
        
        
        blues=np.array(10)
        reds=np.array(10)
        Net=('#0000ff','#3232ff','#4c4cff','#6666ff','#7f7fff','#9999ff','#b2b2ff','#ccccff','#e5e5ff','#ffffff','#fee5e5','#fdcccc','#fcb2b2','#fb9999','#fb7f7f','#fa6666','#f94c4c','#f83232',(1.0, 0, 0.0))
        blues=('#ffffff','#e5e5ff','#ccccff','#b2b2ff','#9999ff','#7f7fff','#6666ff','#4c4cff','#3232ff','#0000ff')
        
        
    
        legColor=Net
        
        if HBlue<0:
            lowestValue=int(HBlue)
        if HBlue>0:
            lowestValue=int(float(HBlue)*float(-1))
        highestValue=int(HRed)
        
#         noOfDivisions=19
#         
#         unit=(highestValue-lowestValue)/noOfDivisions
#         
#         legValue=(range(lowestValue, highestValue, unit))      #range(start, stop[, step])
#         
        
        
        legValue = [lowestValue,0,highestValue]
        bounds=legValue
        
        ax2 = fig.add_axes([0.15, 0.05, 0.15, 0.9])            #dimension of the bar inside the figure for bar (x-coordinate, y-coordinate, length, height)
        cmap = mpl.colors.ListedColormap(legColor)
        cmap.set_over('0.25')
        cmap.set_under('0.75')
      #  bounds = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11]
        norm = mpl.colors.BoundaryNorm(bounds, cmap.N)
        cb2 = mpl.colorbar.ColorbarBase(ax2, cmap=cmap,norm=norm,
                                        #boundaries=[0] + bounds + [13],
                                        #extend='both',
                                        #ticks=bounds,
                                        spacing='proportional',
                                        orientation='vertical')
        cb2.set_label('Number of commuters')
        
        return fig
        #######################Blue#####################
       
        


