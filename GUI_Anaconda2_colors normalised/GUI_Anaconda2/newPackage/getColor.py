# import matplotlib as mpl
# import matplotlib.pyplot as plt
# import matplotlib.colors
# import matplotlib.colorbar
# import numpy as np
# import pandas as pd
# from lxml import etree
# import requests
# import pandas as pd
# import numpy as np
# import matplotlib.pyplot as plt
# import matplotlib.cm as cm
# from matplotlib.colors import Normalize
# from matplotlib.collections import PatchCollection
# import matplotlib.font_manager as fm
# from itertools import chain



# def getColor(no_InCommuters):
#     num_colors=9
#     #values = df[year]                                       #Value of the forest area as percentage of land 
#     value_min=0
#     value_max=100
#     cm = plt.get_cmap('Greens')         #
#     scheme = [cm(i / num_colors) for i in range(num_colors)]
#     bins = np.linspace(value_min, value_max(), num_colors)
#     
#     
#     
#     df['bin'] = np.digitize(no_InCommuters, bins) - 1
#     df.sort_values('bin', ascending=False).head(10)


#Start of Correct Function using if condition that gives Correct red and blue colour to Kriese InCommuters and OutCommuters


def getInColor(no_InCommuters):
    print('get color called')
    if (no_InCommuters>=0 and no_InCommuters<=10): #IN Municipalite Bavaria #change to incommuters
        a='#ffffff'
    elif (no_InCommuters > 10 and no_InCommuters <= 20):
        a = '#fee5e5'
    elif (no_InCommuters > 20 and no_InCommuters <= 30):
        a = '#fdcccc'
    elif (no_InCommuters > 30 and no_InCommuters <= 40):
        a = '#fcb2b'
    elif (no_InCommuters > 40 and no_InCommuters <= 50):
        a = '#fb9999'
    elif (no_InCommuters > 50 and no_InCommuters <= 60):
        a = '#fb7f7f'
    elif (no_InCommuters > 60 and no_InCommuters <= 70):
        a = '#fa6666'
    elif (no_InCommuters > 70 and no_InCommuters <= 80):
        a = '#f94c4c'
    elif (no_InCommuters > 80 and no_InCommuters <= 90):
        a = '#f83232'
    elif (no_InCommuters > 90 and no_InCommuters <= 100):
        a = '#f71919'
    elif (no_InCommuters > 100):
        a = '#f70000'  #Red, Incommuters BadenWurtenberg
    elif (no_InCommuters<0):
        a='silver'
         
    return a

def getOutColor(no_OutCommuters):
    print('get color called')
    if (no_OutCommuters>=0 and no_OutCommuters<=10):     #out municipality Bradenwutternberg
        a='#ffffff'
    elif (no_OutCommuters>10 and no_OutCommuters<=20):
        a='#e5e5ff'
    elif (no_OutCommuters>20 and no_OutCommuters<=30):
        a='#ccccff'
    elif (no_OutCommuters>30 and no_OutCommuters<=40):
        a='#b2b2ff'
    elif (no_OutCommuters>40 and no_OutCommuters<=50):
        a='#9999ff'
    elif (no_OutCommuters>50 and no_OutCommuters<=60):
        a='#7f7fff'
    elif (no_OutCommuters>60 and no_OutCommuters<=70):
        a='#6666ff'
    elif (no_OutCommuters>70 and no_OutCommuters<=80):
        a='#4c4cff'
    elif (no_OutCommuters>80 and no_OutCommuters<=90):
        a='#3232ff'
    elif (no_OutCommuters>90 and no_OutCommuters<=100):
        a='#1919ff'
    elif (no_OutCommuters>100 and no_OutCommuters<=110):
        a='#0000ff'
    elif (no_OutCommuters<0):
        a='silver'
        
    return a

#End of Correct Function using if condition that gives Correct red and blue colour to Kriese InCommuters and OutCommuters
